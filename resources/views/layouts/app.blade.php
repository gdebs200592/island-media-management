<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light fixed-top">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('images/icon/logo.svg') }}" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ url('/') }}">ABOUT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ url('/') }}">MENU</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ url('/') }}">MOODS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ url('/') }}">BLOG</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ url('/') }}">CONTACT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ url('/') }}"><i class="fas fa-search"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="header">
            <div class="header-title">
                <h1>
                    Life begins after Coffee.
                </h1>
            </div>
            <div class="header-cta">
                <button class="btn btn-success header-cta-scrollTo">View Menu</button>
            </div>
            <div class="header-image">
                <img src="{{ asset('images/header-image.jpg') }}" alt="" class="w-100">
            </div>
        </div>

        <main class="py-4">
            @yield('content')
        </main>

        <div class="footer-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <div class="footer-icon">
                            <img src="{{ asset('images/icon/logo.svg') }}" alt="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="footer-address">
                            <p>
                                2800 S White Mountain Rd | Show Low AZ 85901 <br>
                                (928) 537-1425 | info@grinder-coffee.com
                            </p>
                            <a href="{{ url('/') }}"><i class="fab fa-instagram"></i></a> &nbsp; &nbsp;
                            <a href="{{ url('/') }}"><i class="fab fa-facebook-square"></i></a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="footer-newsletter">
                            <p>
                                NEWSLETTER
                            </p>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="YOUR EMAIL ADDRESS" aria-describedby="inputGroupPrepend2" required>
                                <div class="input-group-prepend">
                                  <button type="submit" class="btn btn-warning">SUBSCRIBE</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>
