@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="menu-section text-center">
        <div class="menu-section-title">
            <h1>
                What would you like to have?
            </h1>
        </div>
        <div class="menu-section-description pt-4 pb-5">
            <p>
                Coffee plunger pot sweet barista, grounds acerbic coffee instant crema cream in half and half. Spoon lungo variety, as, siphon, ristretto, iced brewed and acerbic affogato grinder.
            </p>
        </div>
        <div class="menu-section-gallery">
            <div class="row">
                @foreach ($menus as $item)
                <div class="col-md-3">
                    <div class="menu-section-gallery-overlay">
                        <div class="menu-section-gallery-title">
                            <h3>
                                {!! $item->name !!}
                            </h3>
                        </div>
                        <img src="{!! $item->image !!}" alt="" class="w-100">
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="menu-section-extraction mt-5 py-5">
            <div class="menu-section-extraction-title">
                <h1>
                    Extraction instant that variety
                    white robusta strong
                </h1>
            </div>
            <div class="menu-section-extraction-description pt-4">
                <p>
                    Coffee plunger pot sweet barista, grounds acerbic coffee instant crema cream in half and half. Spoon lungo variety, as, siphon, ristretto, iced brewed and acerbic affogato grinder. Mazagran café au lait wings spoon, percolator milk latte dark strong. Whipped, filter latte, filter aromatic grounds doppio caramelization half and half.
                </p>
            </div>
            <div class="menu-section-extraction-cta mt-5">
                <button class="btn btn-warning">CONTACT US</button>
            </div>
        </div>
    </div>
</div>

<div class="benefit-container">
    <div class="container">
        <div class="benefit-section-title text-center">
            <h1>
                Health Benefits of Coffee
            </h1>
        </div>
        <div class="benefit-section-icon mt-5 pt-4">
            <div class="row">
                <div class="col-md-4 text-center">
                    <img src="{{ asset('images/icon/battery-full.svg') }}" alt="" class="benefit-icon-battery">
                    <div class="benefit-section-icon-title pt-4">
                        <h5>
                            BOOST ENERGY LEVEL
                        </h5>
                    </div>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{ asset('images/icon/sun.svg') }}" alt="" class="benefit-icon-sun">
                    <div class="benefit-section-icon-title pt-4">
                        <h5>
                            REDUCE DEPRESSION
                        </h5>
                    </div>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{ asset('images/icon/weight.svg') }}" alt="" class="benefit-icon-weight">
                    <div class="benefit-section-icon-title pt-4">
                        <h5>
                            AID IN WEIGHT LOSS
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="blog-section">
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset('images/blog.jpg') }}" alt="" class="w-100">
            </div>
            <div class="col-md-6">
                <div class="blog-section-content">
                    <h5>
                        BLOG
                    </h5>
                    <div class="blog-section-title">
                        <h3>
                            Qui espresso, grounds to go
                        </h3>
                    </div>
                    <div class="blog-section-post-date">
                        <p>
                            December 12, 2019 | Espresso
                        </p>
                    </div>
                    <div class="blog-section-description">
                        <p>
                            Skinny caffeine aged variety filter saucer redeye, sugar sit steamed eu extraction organic. Beans, crema half and half fair trade carajillo in a variety dripper doppio pumpkin spice cup lungo, doppio, est trifecta breve and, rich, extraction robusta a eu instant. Body sugar steamed, aftertaste, decaffeinated coffee fair trade sit, white shop fair trade galão, dark crema breve frappuccino iced strong siphon trifecta in a at viennese.
                        </p>
                        <a href="{{ url('/') }}">
                            READ MORE <i class="fas fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
