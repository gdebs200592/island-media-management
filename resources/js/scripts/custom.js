// Scroll change bg nav
$(function () {
    $(document).scroll(function () {
        var $navbar = $(".fixed-top");
        $navbar.toggleClass('scrolled', $(this).scrollTop() > $navbar.height());
    });
});

// Smooth scroll
$(function() {
    $(".header-cta-scrollTo").click(function() {
        $('html, body').animate({
            scrollTop: $(".menu-section-title").offset().top -120
        }, 1000);
    });
})