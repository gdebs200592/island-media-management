<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $menus = json_decode('[{
            "id": 1,
            "name": "coffee",
            "image": "/images/gallery-1.jpg"
          }, {
            "id": 2,
            "name": "breakfast",
            "image": "/images/gallery-3.jpg"
          }, {
            "id": 3,
            "name": "sandwich",
            "image": "/images/gallery-4.jpg"
          }, {
            "id": 4,
            "name": "juice",
            "image": "/images/gallery-2.jpg"
          }]
        ');

        return view('home', compact('menus'));
    }
}
